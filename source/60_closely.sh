# OSX-only stuff. Abort if not OSX.
[[ "$OSTYPE" =~ ^darwin ]] || return 1

# list available java versions
alias java_ls='/usr/libexec/java_home -V 2>&1 | grep -E "\d.\d.\d[,_]" | cut -d , -f 1 | colrm 1 4 | grep -v Home'

# switch to a different java version (ie: java_use 1.6)
function java_use() {
    export JAVA_HOME=$(/usr/libexec/java_home -v $1)
    export PATH=$JAVA_HOME/bin:$PATH
    java -version
}

# Java Environment Setup
export JAVA_HOME=$(/usr/libexec/java_home -v 1.6)

# Closely Project Folder setup
export PROJECT_DIR=$HOME/Development/closely
source $PROJECT_DIR/closely-common-scripts/bash/project-aliases

#Groovy/Grails Version Manager (GVM)
#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
[[ -s "$HOME/.gvm/bin/gvm-init.sh" && -z $(which gvm-init.sh | grep '/gvm-init.sh') ]] && source "$HOME/.gvm/bin/gvm-init.sh"
