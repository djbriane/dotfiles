# This file is sourced at the end of a first-time dotfiles install.
shopt -s expand_aliases
source ~/.bashrc

# I'm forgetful. Just look at this repo's commits to see how many times I
# forgot to setup Git and GitHub.

cat <<EOF
SSH Keys (if this is a server)
 1. (main) scp ~/.ssh/id_rsa.pub $USER@$(wanip):~/.ssh/
 2. (here) cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

Closely Dev Environment
 1. Create 'closely' folder to store development projects
 1. Clone git@github.com:lomain/closely-common-scripts.git
 2. Execute closely-common-scripts/download-repos.sh
EOF
